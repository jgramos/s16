// Repetition Control Structures

/*
 3 Typesof looping constructs
 	1. While Loop
 	2. Do-while Loop
 	3. For Loop
*/

/*
	While loop
	-Syntax
		while(expression/condition){
			statements/s;
		}
*/

// let count = 5;

// while(count !== 0){
// 	console.log("While: " + count);
// 	count--; //count = count -1, count =-1;
// }

/*
Expected output:
	While 5
	While 4
	While 3
	While 2
	While 1
*/

/*let count1 = 0;

while(count1 !== 5){
	count1++;
	console.log("While: " + count1);
}*/

/*let num1 = 1;

do {
	console.log("Do-while: " + num1);
	num1++;
} while(num1 <6)

let num2 = Number(prompt("Give me a number"));

do {
	console.log("Do While: " + num2);
	num2 += 1;
}while(num2 < 10)


	For Loop
	Syntax:
		for(initialization; expression/dondition; finalExpression){
		statement;
		}


console.log("For Loop")

for (let i = 1; i <= 5; i++) {
	console.log(i);
}

let newNum  = Number(prompt("Enter a number"));

for(let num4 = newNum; num4 < 10; num4++){
	console.log(num4)
}*/

let newNum  = Number(prompt("Enter a number"));

for(let num5 = 1; num5 != newNum+1; num5++){
	console.log(num5)
}

console.log("String Iteration");

let myString = "Juan";
console.log(myString.length);

console.log(myString[2]);
console.log(myString[3]);

console.log("Displaying individual letters in a string");

for(let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}

console.log("Displaying number 3 for vowels")

let myName = "AloNzO";

for(let i = 0; i < myName.length; i++){
	// if the character of the name is a vowel, instead of displaying the character, display number 3;
	if(
		myName[i].toLowerCase() == 'a' ||
		myName[i].toLowerCase() == 'e' ||
		myName[i].toLowerCase() == 'i' ||
		myName[i].toLowerCase() == 'o' ||
		myName[i].toLowerCase() == 'u'
	){
		console.log(3);
	}
	else{
		console.log(myName[i]);
	}
}

console.log("Continue and Break")

/*
	Continue and Break statements
	 - Continue statement allows the code to go to the next iteration of the loop without finishing the execution of all statents in a code block
	 - Break statement is used to terminate the current loop once a match has been found.
*/

for(let count = 0; count <= 20; count++){
	if(count % 2 === 0){
		continue;
	}

	console.log("Continue and Break: " + count);

	if(count > 10) {
		break;
	}
}
